## Python

```
wget https://www.python.org/ftp/python/3.10.4/Python-3.10.4.tar.xz
sudo apt-get install build-essential gdb lcov pkg-config \
      libbz2-dev libffi-dev libgdbm-dev libgdbm-compat-dev liblzma-dev \
      libncurses5-dev libreadline6-dev libsqlite3-dev libssl-dev \
      lzma lzma-dev tk-dev uuid-dev zlib1g-dev
```

# Poetry

```
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

#OpenSSL

wget https://www.openssl.org/source/openssl-1.1.1b.tar.gz
